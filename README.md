# ssh-routine

I tend to forget these somewhat. Is a handy routine for securing and setting up ssh.

<!-- TOC -->
- [ssh-routine](#ssh-routine)
  - [1. Routine](#1-routine)
  - [2. Commands](#2-commands)
<!-- /TOC -->

## 1. Routine

Things to consider

- set-password
- trying if ssh-copy works

## 2. Commands

```bash
ssh-keygen
cat ~/.ssh/id_rsa.pub | ssh username@remote_host "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"
ssh-add path-to-priv-key
```
Use multiple keys for multiple servers.

Setup 2FA with password and key 


```bash
sudo nano /etc/ssh/sshd_config
```
And insert 

```bash
AuthenticationMethods publickey,password
```